package es.apba.oper.pcs.scp.prechecker.service;

import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;

import es.apba.oper.pcs.scp.prechecker.data.PrecheckerResultData;
import es.apba.oper.pcs.scp.prechecker.data.enums.CompanyEnum;
import es.apba.oper.pcs.scp.prechecker.data.enums.PrecheckerResultEnum;
import es.apba.oper.pcs.scp.prechecker.repositories.PrecheckerResultRepository;
import es.apba.oper.pcs.scp.prechecker.services.PrecheckerResultService;
import es.apba.oper.pcs.scp.prechecker.services.impl.PrecheckerResultResultServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class PrecheckerResultServiceUnitTests
{

    private Map<String, Boolean> companiesSelected = new HashMap<>();
    private final List<String> totalCompanies = Arrays.asList("AML", "BALEARIA", "FRS", "INTERSHIPPING", "TRASMEDITERRANEA");

    private Date startTime = Date.from(Instant.now().minus(366, ChronoUnit.DAYS));
    private Date endTime = Date.from(Instant.now());

    @InjectMocks
    PrecheckerResultService precheckerResultService = new PrecheckerResultResultServiceImpl();

    @Mock
    PrecheckerResultRepository precheckerResultRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addTest()
    {
        PrecheckerResultData precheckerResultData = new PrecheckerResultData();
        precheckerResultData.setNumber("1");

        when(precheckerResultRepository.save(precheckerResultData)).thenReturn(precheckerResultData);

        PrecheckerResultData precheckerResult = precheckerResultService.add(precheckerResultData);

        assertEquals(precheckerResultData.getNumber(), precheckerResult.getNumber());
    }

    @Test
    public void getNotValidPrechecksTest()
    {
        List<PrecheckerResultData> notValidResults = new ArrayList();
        PrecheckerResultData precheckerResultData1 = new PrecheckerResultData();
        precheckerResultData1.setCompany(CompanyEnum.AML);
        precheckerResultData1.setReceivedTime(Instant.now());
        PrecheckerResultData precheckerResultData2 = new PrecheckerResultData();
        precheckerResultData2.setCompany(CompanyEnum.AML);
        precheckerResultData2.setReceivedTime(Instant.now());
        notValidResults.add(precheckerResultData1);
        notValidResults.add(precheckerResultData2);
        totalCompanies.forEach(company -> companiesSelected.put(company, true));

        when(precheckerResultRepository.findByPrecheckerResult(PrecheckerResultEnum.NO_VALID_AGE_CATEGORIES)).thenReturn(notValidResults);

        List<PrecheckerResultData> notValidPrechecks = precheckerResultService.getNotValidPrechecks(companiesSelected, startTime, endTime);

        assertEquals(2, notValidPrechecks.size());
    }

    @Test
    public void getValidPrechecksTest()
    {
        List<PrecheckerResultData> validResults = new ArrayList();
        PrecheckerResultData precheckerResultData = new PrecheckerResultData();
        precheckerResultData.setCompany(CompanyEnum.AML);
        precheckerResultData.setReceivedTime(Instant.now());
        validResults.add(precheckerResultData);
        totalCompanies.forEach(company -> companiesSelected.put(company, true));

        when(precheckerResultRepository.findByPrecheckerResult(PrecheckerResultEnum.OK)).thenReturn(validResults);

        List<PrecheckerResultData> validPrechecks = precheckerResultService.getPrecheckesWithPrecheckesEnum(PrecheckerResultEnum.OK, companiesSelected, startTime, endTime);

        assertEquals(1, validPrechecks.size());
    }
}
