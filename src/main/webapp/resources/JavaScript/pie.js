
function createPie(pieName, pieDescription, values, keys, isMonochrome){

       var options = {
                series: values,
                chart: {
                  height: 325,
                  width: '100%',
                  type: 'pie',
                },
                labels: keys.replace("[", "").replace("]", "").split(", "),
                theme: {
                  monochrome: {
                    enabled: isMonochrome
                  }
                },
                title: {
                  text: pieDescription
                },
                responsive: [{
                  breakpoint: 480,
                  options: {
                    chart: {
                      width: 200
                    },
                    legend: {
                      position: 'bottom'
                    }
                  }
                }]
              };
    var chart = new ApexCharts(document.querySelector("#" + pieName), options);
                  chart.render();
};