package es.apba.oper.pcs.scp.prechecker.services;

import es.apba.oper.pcs.scp.prechecker.data.UserData;

public interface ITeleportSSOService
{
    UserData doLoginWithTeleportSSOUsingSessionId(String sessionId);
    UserData doLoginWithTeleportSSOUsingUsername(String username, String password);
}
