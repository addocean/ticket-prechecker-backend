package es.apba.oper.pcs.scp.prechecker.data.enums;

public enum PrecheckerResultEnum
{
    OK, NO_VALID_TICKETS_NUMBER, NO_VALID_VEHICLES_TYPE, NO_VALID_AGE_CATEGORIES
}
