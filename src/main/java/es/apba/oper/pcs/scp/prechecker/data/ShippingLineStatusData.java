package es.apba.oper.pcs.scp.prechecker.data;

import lombok.Data;

@Data
public class ShippingLineStatusData {

    private Boolean amlStatus;
    private Boolean baleariaStatus;
    private Boolean frsStatus;
    private Boolean intershippingStatus;
    private Boolean trasmediterraneaStatus;

    public ShippingLineStatusData(){

    }

}
