package es.apba.oper.pcs.scp.prechecker.controller;

import es.apba.oper.pcs.scp.prechecker.data.PrecheckerResultData;
import es.apba.oper.pcs.scp.prechecker.services.PrecheckerResultService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.List;

@RestController
@RequestMapping("/prechecker/result")
public class PrecheckerResultController
{
    @Autowired
    PrecheckerResultService precheckerResultService;

    Logger logger = LoggerFactory.getLogger(PrecheckerResultController.class);

    @PostConstruct
    public void postConstruct()
    {
        logger.info("Creating Pre Checker Controller");
    }

    @GetMapping(value = "/")
    public HttpEntity<List<PrecheckerResultData>> getResults()
    {
        return ResponseEntity.ok(precheckerResultService.getAll());
    }

    @PostMapping(value = "/")
    public HttpEntity<String> precheckerControllerAdd(@RequestBody PrecheckerResultData precheckerResultData)
    {
        HttpEntity<String> result;

        logger.info("precheckerControllerAdd " + precheckerResultData.getCompany() + " " + precheckerResultData.getElementType() + " " +
                precheckerResultData.getNumber() + ". RESULT -> " + precheckerResultData.getPrecheckerResult() + " in " + precheckerResultData.getMilliesOfResponse() + " milliseconds");

        precheckerResultData.setReceivedTime(Instant.now());
        PrecheckerResultData precheckerResultDataDB = precheckerResultService.add(precheckerResultData);

        if (precheckerResultDataDB != null)
        {
            result = ResponseEntity.ok().body("Updated " + precheckerResultDataDB.getId());
        }
        else
        {
            result = ResponseEntity.unprocessableEntity().body("Error");
        }

        return result;
    }
}
