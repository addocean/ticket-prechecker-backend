package es.apba.oper.pcs.scp.prechecker.services;

import es.apba.oper.pcs.scp.prechecker.data.ShippingLineStatusData;

public interface ShippingLineStatusService {

    void requestForData();

    ShippingLineStatusData getShippingLineStatusData();

    String getTicketResult(String company, String ticket, boolean isBooking);
}
