package es.apba.oper.pcs.scp.prechecker.data;

import es.apba.oper.pcs.scp.prechecker.data.enums.CompanyEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.Duration;

@Data
@Entity
public class PrecheckerRequestData
{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String bookingNumber;
    private String ticketNumber;

    @NotNull
    private CompanyEnum company;

    @NotNull
    private Duration timeToResponse;
}
