package es.apba.oper.pcs.scp.prechecker.repositories;

import es.apba.oper.pcs.scp.prechecker.data.PrecheckerRequestData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrecheckerRequestRepository extends JpaRepository<PrecheckerRequestData, Long>
{
}
