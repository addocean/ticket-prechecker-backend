package es.apba.oper.pcs.scp.prechecker.controller;

import es.apba.oper.pcs.scp.prechecker.data.ShippingLineStatusData;
import es.apba.oper.pcs.scp.prechecker.services.ShippingLineStatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("/shipping/result")
public class ShippingLineStatusController {

    @Autowired
    ShippingLineStatusService shippingLineStatusService;

    Logger logger = LoggerFactory.getLogger(ShippingLineStatusController.class);

    @PostConstruct
    public void postConstruct(){
        logger.info("Creating Shipping Line Status Controller");
    }

    @GetMapping(value = "/")
    public HttpEntity<ShippingLineStatusData> getResults(){
        return ResponseEntity.ok(shippingLineStatusService.getShippingLineStatusData());
    }
}
