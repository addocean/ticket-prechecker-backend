package es.apba.oper.pcs.scp.prechecker.services.impl;

import es.apba.oper.pcs.scp.prechecker.data.PrecheckerResultData;
import es.apba.oper.pcs.scp.prechecker.data.enums.PrecheckerResultEnum;
import es.apba.oper.pcs.scp.prechecker.repositories.PrecheckerResultRepository;
import es.apba.oper.pcs.scp.prechecker.services.PrecheckerResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class PrecheckerResultResultServiceImpl implements PrecheckerResultService
{
    @Autowired
    PrecheckerResultRepository repository;

    @Override
    public List<PrecheckerResultData> getAll()
    {
        return repository.findAll();
    }

    @Override
    public PrecheckerResultData add(PrecheckerResultData precheckerResultData)
    {
        return repository.save(precheckerResultData);
    }

    @Override
    public List<PrecheckerResultData> getNotValidPrechecks(Map<String, Boolean> companiesSelected, Date startTime, Date endTime)
    {
        List<PrecheckerResultData> notValidResults = new ArrayList<>();
        notValidResults.addAll(getPrecheckesWithPrecheckesEnum(PrecheckerResultEnum.NO_VALID_VEHICLES_TYPE,companiesSelected, startTime, endTime));
        notValidResults.addAll(getPrecheckesWithPrecheckesEnum(PrecheckerResultEnum.NO_VALID_AGE_CATEGORIES, companiesSelected, startTime, endTime));
        notValidResults.addAll(getPrecheckesWithPrecheckesEnum(PrecheckerResultEnum.NO_VALID_TICKETS_NUMBER, companiesSelected, startTime, endTime));
        return notValidResults;
    }

    public List<PrecheckerResultData> getPrecheckesWithPrecheckesEnum(PrecheckerResultEnum precheckerResultEnum, Map<String, Boolean> companiesSelected, Date startTime, Date endTime)
    {
        List<PrecheckerResultData> precheckerList = repository.findByPrecheckerResult(precheckerResultEnum);
        List<PrecheckerResultData> res = new ArrayList<>();
        for (PrecheckerResultData precheckerResultData: precheckerList)
        {
            for (String company : companiesSelected.keySet())
            {
                if (precheckerResultData.getCompany()!=null && precheckerResultData.getCompany().toString().equals(company) && companiesSelected.get(company)
                && startTime.toInstant().atZone(ZoneId.systemDefault()).getDayOfYear()<=(precheckerResultData.getReceivedTime().atZone(ZoneId.systemDefault()).getDayOfYear())
                        && endTime.toInstant().atZone(ZoneId.systemDefault()).getDayOfYear()>=(precheckerResultData.getReceivedTime().atZone(ZoneId.systemDefault()).getDayOfYear()))
                {
                    res.add(precheckerResultData);
                    break;
                }
            }
        }
        return res;
    }
}
