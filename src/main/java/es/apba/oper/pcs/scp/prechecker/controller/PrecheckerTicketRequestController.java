package es.apba.oper.pcs.scp.prechecker.controller;

import es.apba.oper.pcs.scp.prechecker.data.PrecheckerRequestData;
import es.apba.oper.pcs.scp.prechecker.data.enums.CompanyEnum;
import es.apba.oper.pcs.scp.prechecker.services.PrecheckerRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/prechecker/request")
public class PrecheckerTicketRequestController
{
    @Autowired
    PrecheckerRequestService precheckerRequestService;

    private static final Logger logger = LoggerFactory.getLogger(PrecheckerTicketRequestController.class);

    @GetMapping(value = "/{TicketData}")
    public ResponseEntity<PrecheckerRequestData> getTicketData(@RequestParam(value = "ticketNumber", defaultValue = "") String ticketNumber,
                                                               @RequestParam(value = "bookingNumber", defaultValue = "") String bookingNumber,
                                                               @RequestParam(value = "company") CompanyEnum company)
    {
        logger.info("getTicketData Booking={}. Ticket={}. Company={}", bookingNumber, ticketNumber, company);
        PrecheckerRequestData requestData = precheckerRequestService.processRequest(company, ticketNumber, bookingNumber);
        logger.info("getTicketData Booking={}. Ticket={}. Company={}. Processed in={}", bookingNumber, ticketNumber, company, requestData.getTimeToResponse());

        return ResponseEntity.ok(requestData);
    }
}
