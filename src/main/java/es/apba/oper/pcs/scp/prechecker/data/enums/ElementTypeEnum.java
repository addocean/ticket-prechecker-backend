package es.apba.oper.pcs.scp.prechecker.data.enums;

public enum ElementTypeEnum
{
    BOOKING, TICKET, BOARDING
}
