package es.apba.oper.pcs.scp.prechecker.data.enums;

public enum CompanyEnum
{
    ADDOCEAN, AML, BALEARIA, FRS, INTERSHIPPING, TRASMEDITERRANEA
}
