package es.apba.oper.pcs.scp.prechecker.services;

import es.apba.oper.pcs.scp.prechecker.data.PrecheckerResultData;
import es.apba.oper.pcs.scp.prechecker.data.enums.PrecheckerResultEnum;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface PrecheckerResultService
{
    List<PrecheckerResultData> getAll();

    PrecheckerResultData add(PrecheckerResultData precheckerResultData);

    List<PrecheckerResultData> getNotValidPrechecks(Map<String, Boolean> companiesSelected, Date startTime, Date endTime);

    List<PrecheckerResultData> getPrecheckesWithPrecheckesEnum(PrecheckerResultEnum precheckerResultEnum, Map<String, Boolean> companiesSelected, Date startTime, Date endTime);

}
