package es.apba.oper.pcs.scp.prechecker.teleportsso;

import lombok.Data;

@Data
public class TeleportPorts
{
    String nombre;
    String locode;
}
