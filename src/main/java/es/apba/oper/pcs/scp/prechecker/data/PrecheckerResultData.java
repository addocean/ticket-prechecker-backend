package es.apba.oper.pcs.scp.prechecker.data;

import es.apba.oper.pcs.scp.prechecker.data.enums.CompanyEnum;
import es.apba.oper.pcs.scp.prechecker.data.enums.ElementTypeEnum;
import es.apba.oper.pcs.scp.prechecker.data.enums.PrecheckerResultEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.Instant;

@Data
@Entity
public class PrecheckerResultData
{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private CompanyEnum company;
    private ElementTypeEnum elementType;
    private String number;
    private Integer precheckerResult;
    private Long milliesOfResponse;

    private Instant receivedTime;
}
