package es.apba.oper.pcs.scp.prechecker.services.impl;

import es.apba.oper.pcs.scp.prechecker.data.UserData;
import es.apba.oper.pcs.scp.prechecker.teleportsso.UserDataDTOWS;
import org.springframework.stereotype.Service;

@Service
public class UserDataService
{
    public UserData getUserDataFromTeleportResponse(UserDataDTOWS userNameByTeleport)
    {
        UserData userData = null;
        boolean hasPermit = userNameByTeleport.getFuncs().contains("AMV");
        if (hasPermit)
        {
            boolean isAdmin = userNameByTeleport.getFuncs().contains("FCV");
            userData = new UserData();
            userData.setUsername(userNameByTeleport.getUser());
            userData.setAdmin(isAdmin);
            userData.setCompanyName(isAdmin ? userNameByTeleport.getCodusuberman() : "AML");
        }
        return userData;
    }

    public UserData createSimpleUser(String userName)
    {
        UserData userData = new UserData();
        userData.setUsername(userName);
        userData.setAdmin(userName.equalsIgnoreCase("APBA") || userName.equalsIgnoreCase("ADDOCEAN"));
        userData.setCompanyName(userName);
        return userData;
    }
}
