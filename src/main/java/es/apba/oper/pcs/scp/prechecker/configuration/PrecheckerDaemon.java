package es.apba.oper.pcs.scp.prechecker.configuration;

import es.apba.oper.pcs.scp.prechecker.services.ShippingLineStatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Configuration
@Component
@EnableScheduling
public class PrecheckerDaemon {

    private static final Logger log = LoggerFactory.getLogger(PrecheckerDaemon.class);

    @Autowired
    private ShippingLineStatusService shippingLineStatusService;

    @Scheduled(cron = "${prechecker.cron.scheduled.prechecker-daemon}")
    public void precheckerDaemon(){

        log.info("Ejecutando el demonio de prechecker");

        shippingLineStatusService.requestForData();

        log.info("Ejecutando el demonio de prechecker. Fin.");
    }
}
