package es.apba.oper.pcs.scp.prechecker.data;

import lombok.Data;

@Data
public class UserData
{
    private String username;
    private String companyName;
    private boolean isAdmin;
}
