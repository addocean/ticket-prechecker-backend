package es.apba.oper.pcs.scp.prechecker.views;

import es.apba.oper.pcs.scp.prechecker.data.PrecheckerResultData;
import es.apba.oper.pcs.scp.prechecker.data.ShippingLineStatusData;
import es.apba.oper.pcs.scp.prechecker.services.PrecheckerResultService;
import es.apba.oper.pcs.scp.prechecker.services.ShippingLineStatusService;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.annotation.SessionScope;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Data
@ManagedBean
@SessionScope
public class MainView
{
    private int validPrecheckers = 0;
    private int notValidPrecheckers = 0;

    private Map<String, Integer> precheckersTickets = new TreeMap<>();
    private Map<String, Boolean> companiesSelected = new HashMap<>();

    private Map<String, Integer> allPrecheckersTickets = new TreeMap<>();
    private List<PrecheckerResultData> allTickets = new ArrayList<>();

    private final List<String> totalCompanies = Arrays.asList("AML", "BALEARIA", "FRS", "INTERSHIPPING", "TRASMEDITERRANEA");
    private boolean allCompanyFilters;

    private Date startTime = Date.from(Instant.now().minus(366, ChronoUnit.DAYS));
    private Date endTime = Date.from(Instant.now());

    private Map<String, Long> mediaTime24HOfCompanies = new HashMap<>();
    private Map<String, Long> times24Hours = new HashMap<>();
    private Map<String, Long> mediaTime1HOfCompanies = new HashMap<>();
    private Map<String, Long> times1Hour = new HashMap<>();
    private Map<String, Long> mediaTimeLast10OfCompanies = new HashMap<>();
    private Map<String, List<Long>> last10ResponseOfCompanies = new HashMap<>();

    private String noValidAgeCategories = "No valid age categories";
    private String noValidTicketsNumber = "No valid tickets number";
    private String noValidVehiclesType = "No valid vehicles type";
    private String validPrecheckes = "Valid Prechecks";
    private String notValidPrechecks = "Not Valid Prechecks";

    private ShippingLineStatusData shippingLineStatusData;

    private String companySelected;
    private String ticket;
    private String ticketResult;

    Logger logger = LoggerFactory.getLogger(MainView.class);

    @Autowired
    PrecheckerResultService precheckerResultService;

    @Autowired
    ShippingLineStatusService shippingLineStatusService;

    @PostConstruct
    public void init()
    {
        logger.info("postConstruct {}", this);
        initiateFilters();
        allCompanyFilters = false;
        allTickets = precheckerResultService.getAll();
        precheckersTickets.put(noValidAgeCategories, 0);
        precheckersTickets.put(noValidTicketsNumber, 0);
        precheckersTickets.put(noValidVehiclesType, 0);
        allPrecheckersTickets.put(validPrecheckes, 0);
        allPrecheckersTickets.put(notValidPrechecks, 0);
        initiateTimes();
        shippingLineStatusData = shippingLineStatusService.getShippingLineStatusData();
        companySelected = "";
        refresh();
    }

    public void setCompanySelected(String companySelected) {
        this.companySelected = companySelected;
    }

    public void consultTicket(boolean isBooking) {
        ticketResult = shippingLineStatusService.getTicketResult(companySelected, ticket, isBooking);
    }

    public void initiateTimes(){
        totalCompanies.forEach(company -> mediaTime24HOfCompanies.put(company, 0L));
        totalCompanies.forEach(company -> times24Hours.put(company, 0L));
        totalCompanies.forEach(company -> mediaTime1HOfCompanies.put(company, 0L));
        totalCompanies.forEach(company -> times1Hour.put(company, 0L));
        totalCompanies.forEach(company -> mediaTimeLast10OfCompanies.put(company, 0L));
        totalCompanies.forEach(company -> last10ResponseOfCompanies.put(company, new ArrayList<>()));
    }

    public void initiateFilters(){
        totalCompanies.forEach(company -> companiesSelected.put(company, true));
    }

    public void refresh()
    {
        logger.info("refresh");
        if (startTime!=null && endTime!=null && startTime.before(endTime))
        {
            filterTickets();
            filterTimes();
        } else {
            validPrecheckers = 0;
            notValidPrecheckers = 0;
            precheckersTickets = new HashMap<>();
        }
    }

    private void filterTimes() {
        initiateTimes();
        List<PrecheckerResultData> precheckerList = precheckerResultService.getAll();
        Date minus24Hours = Date.from(Instant.now().minus(24, ChronoUnit.HOURS));
        Date minus1Hour = Date.from(Instant.now().minus(1, ChronoUnit.HOURS));
        List<Long> listToAdd;

        for (PrecheckerResultData precheckerResultData: precheckerList)
        {
            int compare24Hours = precheckerResultData.getReceivedTime().compareTo(minus24Hours.toInstant());
            int compare1Hour = precheckerResultData.getReceivedTime().compareTo(minus1Hour.toInstant());
            for (String company : companiesSelected.keySet())
            {
                if (precheckerResultData.getCompany()!=null && precheckerResultData.getCompany().toString().equals(company) && companiesSelected.get(company))
                {
                    clasificaPorHoras(compare24Hours, compare1Hour, company, precheckerResultData);

                    listToAdd = last10ResponseOfCompanies.get(company);
                    listToAdd.add(precheckerResultData.getMilliesOfResponse());
                    last10ResponseOfCompanies.put(company, listToAdd);
                    break;
                }
            }
        }
        getLast10();
        doMedia();
    }

    private void clasificaPorHoras(int compare24Hours, int compare1Hour, String company, PrecheckerResultData precheckerResultData) {
        if (compare24Hours >= 0){
            times24Hours.put(company, times24Hours.get(company)+1);
            mediaTime24HOfCompanies.put(company, mediaTime24HOfCompanies.get(company) + precheckerResultData.getMilliesOfResponse());
            if (compare1Hour >= 0){
                times1Hour.put(company, times1Hour.get(company)+1);
                mediaTime1HOfCompanies.put(company, mediaTime1HOfCompanies.get(company) + precheckerResultData.getMilliesOfResponse());
            }
        }
    }

    private void getLast10() {
        List<Long> res;
        for (String company : companiesSelected.keySet()) {
            res = last10ResponseOfCompanies.get(company);
            res = res.subList(res.size()-Math.min(res.size(), 10), res.size());
            last10ResponseOfCompanies.put(company, res);
        }
    }

    private void doMedia() {
        for (String company : companiesSelected.keySet()){
            if (times24Hours.get(company)!=0) {
                mediaTime24HOfCompanies.put(company, mediaTime24HOfCompanies.get(company) / times24Hours.get(company));
            }
            if (times1Hour.get(company)!=0) {
                mediaTime1HOfCompanies.put(company, mediaTime1HOfCompanies.get(company) / times1Hour.get(company));
            }

            if (!last10ResponseOfCompanies.get(company).isEmpty()) {
                long sum = last10ResponseOfCompanies.get(company).stream().mapToLong(Long::longValue).sum();
                mediaTimeLast10OfCompanies.put(company, sum / last10ResponseOfCompanies.get(company).size());
            }
        }
    }

    public void filterTickets(){
        List<PrecheckerResultData> precheckerList = precheckerResultService.getAll();
        int firstErrorBit = 0x1;
        int secondErrorBit = 0x2;
        int thirdErrorBit = 0x4;
        int fourthErrorBit = 0x08;
        int okTickets = 0;
        int notOkTickets = 0;
        int noValidBoardingTickets = 0;
        int noValidAgeTickets = 0;
        int noValidVehiclesTickets = 0;
        int noValidQuantityTickets = 0;
        resetGraphics();
        for (PrecheckerResultData precheckerResultData: precheckerList)
        {
            for (String company : companiesSelected.keySet())
            {
                if (precheckerResultData.getCompany()!=null && precheckerResultData.getCompany().toString().equals(company) && companiesSelected.get(company)
                        && startTime.toInstant().atZone(ZoneId.systemDefault()).getDayOfYear()<=(precheckerResultData.getReceivedTime().atZone(ZoneId.systemDefault()).getDayOfYear())
                        && endTime.toInstant().atZone(ZoneId.systemDefault()).getDayOfYear()>=(precheckerResultData.getReceivedTime().atZone(ZoneId.systemDefault()).getDayOfYear()))
                {
                    if (precheckerResultData.getPrecheckerResult() == 0) {
                        okTickets++;
                    } else {
                        notOkTickets++;
                        if ((precheckerResultData.getPrecheckerResult() & firstErrorBit) != 0) {
                            noValidBoardingTickets++;
                        }
                        if ((precheckerResultData.getPrecheckerResult() & secondErrorBit) != 0) {
                            noValidAgeTickets++;
                        }
                        if ((precheckerResultData.getPrecheckerResult() & thirdErrorBit) != 0) {
                            noValidVehiclesTickets++;
                        }
                        if ((precheckerResultData.getPrecheckerResult() & fourthErrorBit) != 0) {
                            noValidQuantityTickets++;
                        }
                    }
                    allPrecheckersTickets.put(validPrecheckes, okTickets);
                    allPrecheckersTickets.put(notValidPrechecks, notOkTickets);
                    precheckersTickets.put("No valid boarding", noValidBoardingTickets);
                    precheckersTickets.put(noValidAgeCategories, noValidAgeTickets);
                    precheckersTickets.put(noValidTicketsNumber, noValidQuantityTickets);
                    precheckersTickets.put(noValidVehiclesType, noValidVehiclesTickets);

                    break;
                }
            }
        }
    }

    public void resetGraphics() {
        allPrecheckersTickets.put(validPrecheckes, 0);
        allPrecheckersTickets.put(notValidPrechecks, 0);
        precheckersTickets.put("No valid boarding", 0);
        precheckersTickets.put(noValidAgeCategories, 0);
        precheckersTickets.put(noValidTicketsNumber, 0);
        precheckersTickets.put(noValidVehiclesType, 0);
    }

    public void activateAllFilters(){
        if(allCompanyFilters){
            totalCompanies.forEach(company -> companiesSelected.put(company, true));
        } else {
            totalCompanies.forEach(company -> companiesSelected.put(company, false));
        }
        refresh();
    }
}
