package es.apba.oper.pcs.scp.prechecker.services.impl;

import es.apba.oper.pcs.scp.prechecker.data.ShippingLineStatusData;
import es.apba.oper.pcs.scp.prechecker.services.ShippingLineStatusService;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import java.security.cert.X509Certificate;
import java.util.*;

@Service
public class ShippingLineStatusServiceImpl implements ShippingLineStatusService {

    private ShippingLineStatusData shippingLineStatusData;

    private static final String BALEARIA = "BALEARIA";
    private static final String AML = "AML";
    private static final String FRS = "FRS";
    private static final String INTERSHIPPING = "INTERSHIPPING";
    private static final String TRASMEDITERRANEA = "TRASMEDITERRANEA";
    private static final String ADDOCEAN_1 = "ADDOCEAN_1";
    private static final String ADDOCEAN_2 = "ADDOCEAN_2";
    private static final String ADDOCEAN_3 = "ADDOCEAN_3";

    private final List<String> companiesToRequestShippingLineStatusData = Arrays.asList(TRASMEDITERRANEA, AML, BALEARIA, FRS, INTERSHIPPING, ADDOCEAN_1, ADDOCEAN_2, ADDOCEAN_3);

    @Value("${prechecker.url." + AML + "}")
    private String amlUrl;

    @Value("${prechecker.token." + AML + "}")
    private String amlToken;

    @Value("${prechecker.url." + BALEARIA + "}")
    private String baleariaUrl;

    @Value("${prechecker.token." + BALEARIA + "}")
    private String baleariaToken;

    @Value("${prechecker.url." + FRS + "}")
    private String frsUrl;

    @Value("${prechecker.token." + FRS + "}")
    private String frsToken;

    @Value("${prechecker.url." + INTERSHIPPING + "}")
    private String intershippingUrl;

    @Value("${prechecker.token." + INTERSHIPPING + "}")
    private String intershippingToken;

    @Value("${prechecker.url." + TRASMEDITERRANEA +"}")
    private String trasmediterraneaUrl;

    @Value("${prechecker.token." + TRASMEDITERRANEA + "}")
    private String trasmediterraneaToken;

    @Value("${prechecker.url." + ADDOCEAN_1 + "}")
    private String addocean1Url;

    @Value("${prechecker.token." + ADDOCEAN_1 + "}")
    private String addocean1Token;

    @Value("${prechecker.url." + ADDOCEAN_2 + "}")
    private String addocean2Url;

    @Value("${prechecker.token." + ADDOCEAN_2 + "}")
    private String addocean2Token;

    @Value("${prechecker.url." + ADDOCEAN_3 + "}")
    private String addocean3Url;

    @Value("${prechecker.token." + ADDOCEAN_3 + "}")
    private String addocean3Token;

    @Value("${prechecker.post." + AML + "}")
    private boolean amlPost;

    @Value("${prechecker.post." + BALEARIA + "}")
    private boolean baleariaPost;

    @Value("${prechecker.post." + INTERSHIPPING + "}")
    private boolean interShippingPost;

    @Value("${prechecker.post." + FRS + "}")
    private boolean frsPost;

    @Value("${prechecker.post." + TRASMEDITERRANEA + "}")
    private boolean trasmediterraneaPost;

    @Value("${prechecker.post." + ADDOCEAN_1 + "}")
    private boolean addocean1Post;

    @Value("${prechecker.post." + ADDOCEAN_2 + "}")
    private boolean addocean2Post;

    @Value("${prechecker.post." + ADDOCEAN_3 + "}")
    private boolean addocean3Post;

    private static final String HEADER_ACCEPT = "Accept";
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String CONTENT_TYPE = "Content-Type";

    private final Map<String, String> urls = new HashMap<>();
    private final Map<String, String> tokens = new HashMap<>();
    private final Map<String, Boolean> post = new HashMap<>();

    private static final Logger log = LoggerFactory.getLogger(ShippingLineStatusServiceImpl.class);

    @PostConstruct
    public void init(){
        shippingLineStatusData = new ShippingLineStatusData();
        urls.put(AML, amlUrl);
        urls.put(BALEARIA, baleariaUrl);
        urls.put(FRS, frsUrl);
        urls.put(INTERSHIPPING, intershippingUrl);
        urls.put(TRASMEDITERRANEA, trasmediterraneaUrl);
        urls.put(ADDOCEAN_1, addocean1Url);
        urls.put(ADDOCEAN_2, addocean2Url);
        urls.put(ADDOCEAN_3, addocean3Url);
        tokens.put(AML, amlToken);
        tokens.put(BALEARIA, baleariaToken);
        tokens.put(FRS, frsToken);
        tokens.put(INTERSHIPPING, intershippingToken);
        tokens.put(TRASMEDITERRANEA, trasmediterraneaToken);
        tokens.put(ADDOCEAN_1, addocean1Token);
        tokens.put(ADDOCEAN_2, addocean2Token);
        tokens.put(ADDOCEAN_3, addocean3Token);
        post.put(AML, amlPost);
        post.put(BALEARIA, baleariaPost);
        post.put(FRS, frsPost);
        post.put(INTERSHIPPING, interShippingPost);
        post.put(TRASMEDITERRANEA, trasmediterraneaPost);
        post.put(ADDOCEAN_1, addocean1Post);
        post.put(ADDOCEAN_2, addocean2Post);
        post.put(ADDOCEAN_3, addocean3Post);
    }

    public ShippingLineStatusData getShippingLineStatusData() {
        return shippingLineStatusData;
    }

    public void requestForData(){
        companiesToRequestShippingLineStatusData.stream().filter(this::isValidCompany).forEach(this::getShippingStatus);
    }

    public String getTicketResult(String company, String ticket, boolean isBooking) {
        boolean doPost = post.get(company);
        if (doPost) {
            ResponseEntity<Object[]> restResult = postForShippingLineStatusToCompany(company, ticket, isBooking);
            if (restResult != null && restResult.getBody() != null) {
                return restResult.getBody().toString();
            }
        } else {
            ResponseEntity<Object> restResult = requestForShippingLineStatusToCompany(company, ticket, isBooking);
            if (restResult != null && restResult.getBody() != null) {
                return restResult.getBody().toString();
            }
        }
        return "";
    }

    private void getShippingStatus(String company) {
        boolean doPost = post.get(company);
        if (doPost) {
            ResponseEntity<Object[]> restResult = postForShippingLineStatusToCompany(company, "000000000000000", true);
            if (restResult != null) {
                setStatus(restResult.getStatusCode().is2xxSuccessful(), company);
            }
        } else {
            ResponseEntity<Object> restResult = requestForShippingLineStatusToCompany(company, "000000000000000", true);
            if (restResult != null) {
                // AML devuelve un 441 como indicación de que el ticket no existe, se considera una respuesta válida
                if (restResult.getStatusCodeValue() == 441)
                {
                    setStatus(true, company);
                    log.info("{} ha dado 441 OK", company);
                }
                else if (restResult.getStatusCodeValue() == 444)
                {
                    setStatus(false, company);
                    log.info("{} ha dado 444", company);
                }
                else
                {
                    setStatus(restResult.getStatusCode().is2xxSuccessful(), company);
                    log.info("{} ha dado 2xx OK", company);
                }
            }
        }
    }

    private ResponseEntity<Object[]> postForShippingLineStatusToCompany(String company, String ticket, boolean isBooking){
        ResponseEntity<Object[]> restResult = null;
        try {
            log.info("postForShippingLinesStatusToCompany {}", company);

            RestTemplate restTemplate = getRestTemplate();

            HttpHeaders header = new HttpHeaders();
            header.set(HEADER_ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            header.set(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            header.set(HEADER_AUTHORIZATION, "Basic " + getBearerForCompany(company));

            String requestJson = "";
            if (isBooking) {
                requestJson = "{\"ticketNumber\": \"" + 0 + "\"" +
                        ", \"bookingNumber\": \"" + ticket + "\"}";
            } else {
                requestJson = "{\"ticketNumber\": \"" + ticket + "\"" +
                        ", \"bookingNumber\": \"" + 0 + "\"}";
            }

            HttpEntity<?> entity = new HttpEntity<>(requestJson, header);

            String urlCompleta = getURLForCompany(company);

            restResult = restTemplate.postForEntity(urlCompleta, entity, Object[].class);

            log.info(company + " ha dado 2XX OK");
        }
        catch (HttpClientErrorException.NotFound ex1){
            setStatus(true, company);
            log.info(company + " ha dado error 404 Not Found");
        }
        catch (RestClientException | IllegalArgumentException ex){
            log.error(ex.getMessage());
            setStatus(false, company);
        }
        return restResult;
    }

    private ResponseEntity<Object> requestForShippingLineStatusToCompany(String company, String ticket, boolean isBooking) {
        ResponseEntity<Object> restResult = null;
        try {
            log.info("requestForShippingLinesStatusToCompany {}", company);

            RestTemplate restTemplate = getRestTemplate();

            HttpHeaders header = new HttpHeaders();
            header.set(HEADER_ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            header.set(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            header.set(HEADER_AUTHORIZATION, "Basic " + getBearerForCompany(company));

            HttpEntity<?> entity = new HttpEntity<>(header);
            String urlCompleta = "";
            if (isBooking) {
                urlCompleta = getURLForCompany(company) + "?tickerNumber=" + "000000000000000" + "&bookingNumber=" + ticket + "&departurePort=ESALG";
            } else {
                urlCompleta = getURLForCompany(company) + "?tickerNumber=" + ticket + "&bookingNumber=" + "000000000000000" + "&departurePort=ESALG";
            }

            restResult = restTemplate.exchange(urlCompleta, HttpMethod.GET, entity, Object.class);

        }
        catch (HttpClientErrorException.NotFound ex1){
            setStatus(true, company);
            log.info(company + " ha dado error 404 Not Found");
        }
        catch (RestClientException ex){
            log.error(ex.getMessage());
            setStatus(false, company);
        }

        return restResult;
    }

    public void setStatus(Boolean isOk, String company){
            switch (company) {
                case AML:
                    shippingLineStatusData.setAmlStatus(isOk);
                    break;
                case BALEARIA:
                    shippingLineStatusData.setBaleariaStatus(isOk);
                    break;
                case FRS:
                    shippingLineStatusData.setFrsStatus(isOk);
                    break;
                case INTERSHIPPING:
                    shippingLineStatusData.setIntershippingStatus(isOk);
                    break;
                case TRASMEDITERRANEA:
                    shippingLineStatusData.setTrasmediterraneaStatus(isOk);
                    break;
                default:
            }
    }

    private RestTemplate getRestTemplate()
    {
        RestTemplate restTemplate = new RestTemplate();

        try {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

            SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();

            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

            CloseableHttpClient httpClient = HttpClients.custom()
                    .setSSLSocketFactory(csf)
                    .build();

            HttpComponentsClientHttpRequestFactory requestFactory =
                    new HttpComponentsClientHttpRequestFactory();

            requestFactory.setHttpClient(httpClient);
            requestFactory.setConnectTimeout(60000);
            requestFactory.setConnectionRequestTimeout(60000);
            requestFactory.setReadTimeout(60000);

            restTemplate = new RestTemplate(requestFactory);

        } catch (Exception ex) {
            log.info(String.valueOf(Arrays.asList(ex.getStackTrace())));
        }

        return restTemplate;
    }

    private String getBearerForCompany(String company) {
        return tokens.getOrDefault(company, "");
    }

    private String getURLForCompany(String company) {
        return urls.getOrDefault(company, "");
    }

    private boolean isValidCompany(String company) {
        String companyUrl = urls.getOrDefault(company, "");
        return companyUrl != null && !companyUrl.trim().isEmpty();
    }


}
