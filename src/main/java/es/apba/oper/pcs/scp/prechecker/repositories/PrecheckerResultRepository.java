package es.apba.oper.pcs.scp.prechecker.repositories;

import es.apba.oper.pcs.scp.prechecker.data.PrecheckerResultData;
import es.apba.oper.pcs.scp.prechecker.data.enums.PrecheckerResultEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrecheckerResultRepository extends JpaRepository<PrecheckerResultData, Long>
{
    List<PrecheckerResultData> findByPrecheckerResult(PrecheckerResultEnum precheckerResult);

    List<PrecheckerResultData> findByPrecheckerResultIn(List<PrecheckerResultEnum> notValidResults);

}
