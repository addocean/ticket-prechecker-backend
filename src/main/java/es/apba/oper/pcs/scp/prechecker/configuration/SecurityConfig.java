package es.apba.oper.pcs.scp.prechecker.configuration;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception{

        http.authorizeRequests()
                .antMatchers("/javax.faces.resource/**").permitAll()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/prechecker/result/**").permitAll()
                .antMatchers("/shipping/result/**").permitAll();

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/login*").permitAll()
                .antMatchers("/mainView*").authenticated()
                .antMatchers(HttpMethod.POST, "/mainView*").authenticated()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().accessDeniedPage("/accessDenied.xhtml")
                .and()
                .formLogin()
                .loginPage("/login.xhtml")
                .defaultSuccessUrl("/mainView.xhtml", true)
                .failureUrl("/login.xhtml?error=true")
                .and()
                .logout()
                .logoutSuccessUrl("/login.xhtml")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .and()
                .sessionManagement()
                .invalidSessionUrl("/expired")
                .maximumSessions(1)
                .expiredUrl("/expired");

    }
}
