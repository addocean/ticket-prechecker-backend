package es.apba.oper.pcs.scp.prechecker.services;

import es.apba.oper.pcs.scp.prechecker.data.PrecheckerRequestData;
import es.apba.oper.pcs.scp.prechecker.data.enums.CompanyEnum;

public interface PrecheckerRequestService
{
    PrecheckerRequestData processRequest(CompanyEnum company, String ticketNumber, String bookingNumber);
}
