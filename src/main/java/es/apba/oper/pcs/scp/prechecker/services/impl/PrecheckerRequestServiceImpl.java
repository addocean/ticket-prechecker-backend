package es.apba.oper.pcs.scp.prechecker.services.impl;

import es.apba.oper.pcs.scp.prechecker.data.PrecheckerRequestData;
import es.apba.oper.pcs.scp.prechecker.data.enums.CompanyEnum;
import es.apba.oper.pcs.scp.prechecker.repositories.PrecheckerRequestRepository;
import es.apba.oper.pcs.scp.prechecker.services.PrecheckerRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;

@Service
public class PrecheckerRequestServiceImpl implements PrecheckerRequestService
{

    @Autowired
    PrecheckerRequestRepository requestRepository;

    @Override
    public PrecheckerRequestData processRequest(CompanyEnum company, String ticketNumber, String bookingNumber)
    {
        PrecheckerRequestData result = new PrecheckerRequestData();
        result.setBookingNumber(bookingNumber);
        result.setTicketNumber(ticketNumber);
        result.setCompany(company);

        LocalDateTime initialTime = LocalDateTime.now();
        LocalDateTime endTime = LocalDateTime.now();
        Duration duration = Duration.between(initialTime, endTime);
        result.setTimeToResponse(duration);

        requestRepository.save(result);

        return result;
    }
}
